

@extends('base.index')


@section('content')

<!--=================================
 banner -->

<section class="inner-intro bg bg-fixed bg-overlay-black-60" style="background-image:url(images/bg/inner-bg-1.jpg);">
    <div class="container">
        <div class="row intro-title text-center">
            <div class="col-sm-12">
                <div class="section-title"><h1 class="pos-r divider">Pricing<span class="sub-title">data-table</span></h1></div>
            </div>
            <div class="col-sm-12 mt-70">
                <ul class="page-breadcrumb">
                    <li><a href="/"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                    <li><a href="elements-data-table.html"> Pricing</a> <i class="fa fa-angle-double-right"></i></li>
                    <li><span>Data-table</span> </li>
                </ul>
            </div>
        </div>

    </div>
</section>

<!--=================================
 inner-intro-->


<!--=================================
table -->

@include('base.component.pricing')

<!--=================================
 inner-intro-->
@endsection