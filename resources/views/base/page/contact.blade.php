
@extends('base.index')


@section('content')



<!--=================================
 banner -->


<section class="login-form dark-bg page-section-ptb bg-overlay-black-30 bg" style="background: url(images/pattern/02.png) no-repeat 0 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-1-form clearfix text-center">
                    <h4 class="title divider-3 text-white">SIGN IN</h4>
                    <div class="login-1-social mt-50 mb-50 text-center clearfix">
                        <ul class="list-inline text-capitalize">
                            <li><a class="fb" href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
                            <li><a class="gplus" href="#"><i class="fa fa-google-plus"></i>  google+</a></li>
                        </ul>
                    </div>
                    <div class="section-field mb-20">
                        <div class="field-widget">
                            <i class="glyph-icon flaticon-user"></i>
                            <input id="name" class="web" type="text" placeholder="User name" name="web">
                        </div>
                    </div>
                    <div class="section-field mb-10">
                        <div class="field-widget">
                            <i class="glyph-icon flaticon-padlock"></i>
                            <input id="Password" class="Password" type="password" placeholder="Password" name="Password">
                        </div>
                    </div>
                    <div class="section-field text-uppercase">
                        <a href="#" class="pull-right text-white">Forgot Password?</a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="section-field text-uppercase text-right mt-20">
                        <a class="button  btn-lg btn-theme full-rounded animated right-icn"><span>sign in<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a>
                    </div><div class="clearfix"></div>
                    <div class="section-field mt-20 text-center text-white">
                        <p class="lead mb-0">Don’t have an account? <a class="text-white" href="register.html"><u>Register now!</u> </a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<!--=================================
 login-->




@endsection