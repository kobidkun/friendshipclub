
@extends('base.index')


@section('content')


<!--=================================
 banner -->
@include('base.component.slider')

<!--=================================
 banner -->


<!--=================================
 Page Section -->

<section class="form-1 ptb-20">
    <div class="container">
        <div class="row">
            <div class="banner-form">
                <form
                        role="form" method="post" action="{{ route('form') }}"
                >

                    {{ csrf_field() }}


                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="col-md-2 col-sm-12 control-label text-white">Name</label>
                            <div class="col-md-10 col-sm-12">
                                <input type="text" name="name" class="form-control" id="InputName" placeholder="Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="col-md-2 col-sm-12 control-label text-white">Mobile</label>
                            <div class="col-md-10 col-sm-12">
                                <input type="text"  name="mobile" class="form-control" id="InputName" placeholder="Mobile">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">

                                    <div class="col-md-12 col-sm-12">
                                        <input type="text"  name="gender" class="form-control" id="InputName" placeholder="Gender">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">

                                    <div class="col-md-12 col-sm-12">
                                        <input type="text" name="city" class="form-control" id="InputName" placeholder="City">
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-2"><button type="submit" class="button btn-lg btn-block btn-theme full-rounded animated right-icn">
                            <span style="text-align: center">Register <i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></button></div>
                </form>
            </div>
        </div>
    </div>
</section>


<section class="page-section-ptb pos-r timeline-section">
    <div class="container">
        <div class="row mb-50 xs-mb-30">
            <div class="col-md-offset-1 col-md-10 text-center">
                <h2 class="title divider mb-30">Step to find your Soul Mate</h2>
                <p class="lead">

                    Find your Solemate  Easily
                    <br>


                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10 col-md-12">
                <ul class="timeline list-inline">
                    <li>
                        <div class="timeline-badge"><img class="img-responsive" src="images/timeline/01.png" alt=""></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading text-center">
                                <h4 class="timeline-title divider-3">CREATE PROFILE</h4>
                            </div>
                            <div class="timeline-body">
                                <p>
                                    Start by Registering with us its free and Easy

                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge"><img class="img-responsive" src="images/timeline/02.png" alt=""></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading text-center">
                                <h4 class="timeline-title divider-3">Find match</h4>
                            </div>
                            <div class="timeline-body">
                                <p>
                                    Easily find perfect match by yourself just by entering your requirements

                                </p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge"><img class="img-responsive" src="images/timeline/03.png" alt=""></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading text-center">
                                <h4 class="timeline-title divider-3">START DATING</h4>
                            </div>
                            <div class="timeline-body">
                                <p>
                                 Yeah!!   All done. Start Dating

                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div></div>

</section>
@include('base.component.pricing')
<section class="page-section-ptb  text-white" style="background: url(images/pattern/02.png) no-repeat 0 0; background-size: cover;">
    <div class="container">
        <div class="row mb-50 xs-mb-30">
            <div class="col-md-offset-1 col-md-10 text-center">
                <h2 class="title divider"> Facts</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-3 col-xs-6 col-xx-12 text-center">
                <div class="counter">
                    <img src="images/counter/01.png" alt="">
                    <span class="timer" data-to="1600" data-speed="10000">1600</span><label>Total Members</label>
                </div>
            </div>
            <div class="col-lg-3 col-sm-3 col-xs-6 col-xx-12 text-center">
                <div class="counter">
                    <img src="images/counter/02.png" alt="">
                    <span class="timer" data-to="750" data-speed="10000">750</span><label>Online Members</label>
                </div>
            </div>
            <div class="col-lg-3 col-sm-3 col-xs-6 col-xx-12 text-center">
                <div class="counter">
                    <img src="images/counter/03.png" alt="">
                    <span class="timer" data-to="380" data-speed="10000">380</span><label>Men Online</label>
                </div>
            </div>
            <div class="col-lg-3 col-sm-3 col-xs-6 col-xx-12 text-center">
                <div class="counter">
                    <img src="images/counter/04.png" alt="">
                    <span class="timer" data-to="370" data-speed="10000">370</span><label>Women Online</label>
                </div>
            </div>
        </div>
    </div>
</section>


@include('base.component.profile')





<section class="page-section-ptb o-hidden grey-bg">
    <div class="container">
        <div class="row mb-50 xs-mb-0">
            <div class="col-md-offset-1 col-md-10 text-center">
                <h2 class="title divider mb-30">Why Choose Us</h2>
                <p class="lead">

                    charming is a quality friendship and dating site to find new people online for dating & friendship..
                    <br>
                    2.Joining web-deal group is a simple way to find love and friends, search for dating, friendship.
                    We are providing dating and friendship to help people to find new friends in their local or around the globe.
                    <br>
                    charming is medium through which you find out your partner easily according to your choice, your desire a partner who would understand you, support you, would share your feelings & everything that you deserve.we try to our best effort to match the profiles what you want.we are providing 100% genuine service and we are the only dating site that provides you genuine service...
                    <br>
                    So come & join with us to create one profile and see how easy to make new friends online at Web-deal group. Create your profile today and change your life tomorrow by joining....






                </p>
            </div>
        </div>


    </div>
</section>


<!--=================================
 page-section -->

<section

        class="ptb-50 action-box-img bg mt-100 mb-100 text-center text-white bg-overlay-black-80"
        style="background-image:url(images/bg/bg-5.jpg);padding-top: -500px"
>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h5 class="pb-20">Want to hear more story, subscribe for our newsletter</h5>
                <a href="#" class="button  btn-lg btn-theme full-rounded animated right-icn"><span>Subscribe<i class="glyph-icon flaticon-hearts" aria-hidden="true"></i></span></a>
            </div>
        </div>

    </div>
</section>

@endsection