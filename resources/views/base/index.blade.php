<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.potenzaglobalsolutions.com/html/cupid-love-dating-website-html5-template/index-default.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 18 Feb 2018 19:20:33 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Charming Friendship Club" />
    <meta name="description" content="Charming Friendship Club" />
    <meta name="author" content="Charming Friendship Club" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Welcome to Charming Friendship Club</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico" />

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

    <!-- mega menu -->
    <link href="css/mega-menu/mega_menu.css" rel="stylesheet" type="text/css" />

    <!-- font-awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Flaticon -->
    <link href="css/flaticon.css" rel="stylesheet" type="text/css" />

    <!-- Magnific popup -->
    <link href="css/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />

    <!-- owl-carousel -->
    <link href="css/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />

    <!-- General style -->
    <link href="css/general.css" rel="stylesheet" type="text/css" />

    <!-- main style -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <!-- Style customizer -->
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="#" data-style="styles" />
    <link rel="stylesheet" type="text/css" href="css/style-customizer.css" />

</head>

<body>

<!--=================================
 preloader -->

<div id="preloader">
    <div class="clear-loading loading-effect"><img src="images/loading.gif" alt=""></div>
</div>
<!--=================================
header -->

@include('base.component.header')

<!--=================================
 header -->

@yield('content')


<!--=================================
footer -->

@include('base.component.footer')
<!--=================================
footer -->

<!--=================================
Color Customizer -->


<div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-level-up"></i></a></div>

<!--=================================
 jquery -->


<!-- jquery  -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- bootstrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>

<!-- appear -->
<script type="text/javascript" src="js/jquery.appear.js"></script>

<!-- Menu -->
<script type="text/javascript" src="js/mega-menu/mega_menu.js"></script>

<!-- owl-carousel -->
<script type="text/javascript" src="js/owl-carousel/owl.carousel.min.js"></script>

<!-- counter -->
<script type="text/javascript" src="js/counter/jquery.countTo.js"></script>

<!-- Magnific Popup -->
<script type="text/javascript" src="js/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- style customizer  -->
<script type="text/javascript" src="js/style-customizer.js"></script>

<!-- custom -->
<script type="text/javascript" src="js/custom.js"></script>


</body>

<!-- Mirrored from themes.potenzaglobalsolutions.com/html/cupid-love-dating-website-html5-template/index-default.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 18 Feb 2018 19:23:39 GMT -->
</html>