<footer class="page-section-pt text-white text-center">


    <div class="footer-widget mt-50">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="footer-logo mb-20">
                        <img class="img-center" src="images/footer-logo.png" alt="">
                    </div>

                    <div class="social-icons color-hover mb-10">
                        <ul>
                            <li class="social-facebook"><a href="https://www.facebook.com/char.ming.52035" target="_blank"><i class="fa fa-facebook"></i></a></li>

                        </ul>
                    </div>

                    <p class="text-white">© 2017 <a href="{{route('policy')}}">Terms & Condition | Privacy Policy | Refund Policy</a> </p>
                </div>
            </div>
        </div></div>

</footer>
