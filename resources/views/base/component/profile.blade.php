<section class="page-section-ptb profile-slider pb-30 xs-pb-60">
    <div class="container">
        <div class="row mb-20 xs-mb-0">
            <div class="col-md-offset-1 col-md-10 text-center">
                <h2 class="title divider">Last Added Profiles</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="owl-carousel" data-nav-arrow="true" data-items="4" data-md-items="4" data-sm-items="3" data-xs-items="2" data-xx-items="1" data-space="30">
                    <div class="item">
                        <a href="#" class="profile-item">
                            <div class="profile-image clearfix"><img class="img-responsive" src="profile/1.jpeg" alt=""></div>
                            <div class="profile-details text-center">
                                <h5 class="title">Seema</h5>
                                <span>21 Years Old</span>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#" class="profile-item">
                            <div class="profile-image clearfix"><img class="img-responsive" src="profile/8.jpeg" alt=""></div>
                            <div class="profile-details text-center">
                                <h5 class="title">Amit Verma</h5>
                                <span>21 Years Old</span>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#" class="profile-item">
                            <div class="profile-image clearfix"><img class="img-responsive" src="profile/2.jpeg" alt=""></div>
                            <div class="profile-details text-center">
                                <h5 class="title">Aisha Gupta</h5>
                                <span>26 Years Old</span>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" class="profile-item">
                            <div class="profile-image clearfix"><img class="img-responsive" src="profile/3.jpeg" alt=""></div>
                            <div class="profile-details text-center">
                                <h5 class="title">Susmita Dutt</h5>
                                <span>23 Years Old</span>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" class="profile-item">
                            <div class="profile-image clearfix"><img class="img-responsive" src="profile/4.jpeg" alt=""></div>
                            <div class="profile-details text-center">
                                <h5 class="title">Sanjana Das</h5>
                                <span>29 Years Old</span>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="profile-details.html" class="profile-item">
                            <div class="profile-image clearfix"><img class="img-responsive" src="profile/5.jpeg" alt=""></div>
                            <div class="profile-details text-center">
                                <h5 class="title">Gauri Khan</h5>
                                <span>27 Years Old</span>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="profile-details.html" class="profile-item">
                            <div class="profile-image clearfix"><img class="img-responsive" src="profile/6.jpeg" alt=""></div>
                            <div class="profile-details text-center">
                                <h5 class="title">Sid</h5>
                                <span>24 Years Old</span>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="profile-details.html" class="profile-item">
                            <div class="profile-image clearfix"><img class="img-responsive" src="profile/7.jpeg" alt=""></div>
                            <div class="profile-details text-center">
                                <h5 class="title">Anuj Verma</h5>
                                <span>32 Years Old</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>