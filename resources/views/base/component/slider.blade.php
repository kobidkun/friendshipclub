<section id="home-slider" class="fullscreen">
    <div id="main-slider" class="carousel carousel-fade" data-ride="carousel">
        <!-- Carousel inner -->
        <div class="carousel-inner">
            <!--/ Carousel item end -->

            <div class="item bg-1 active" style="background: url(images/bg/bg-1.jpg) no-repeat 0 0; background-size: cover;" data-gradient-red="4">
                <div class="slider-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="slider-1">
                                    <h1 class="animated2 text-white">Are You <span>Waiting</span> For <span> Dating ?</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item bg-1" style="background: url(images/bg/bg-2.jpg) no-repeat 0 0; background-size: cover;" data-gradient-red="4">
                <div class="slider-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-left">
                                <div class="slider-2">
                                    <h1 class="animated7 text-white">Meet big <span> and </span> beautiful love <span> here!</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item bg-1" style="background: url(images/bg/bg-3.jpg) no-repeat 0 0; background-size: cover;" data-gradient-red="4">
                <div class="slider-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-left">
                                <div class="slider-3">
                                    <h1 class="animated3 text-white">The Right <span>Place</span> to Meet Your <span> Soul Mate!</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Carousel item end -->
        </div>

    </div>
</section>