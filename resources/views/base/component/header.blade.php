








<header id="header" class="dark">

    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="topbar-left text-left">
                        <ul class="list-inline">

                            <li><a href="tel:(007)1234567890"><i class="fa fa-phone"></i>
                                    7718232689
                                    7718212406
                                    7718230833
                                </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="topbar-right text-right">
                        <ul class="list-inline social-icons color-hover">
                            <li class="social-facebook"><a href="https://www.facebook.com/char.ming.52035" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                        <ul class="list-inline text-uppercase top-menu">
                            <li><a href="#">register</a></li>
                            <li><a href="#">login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=================================
     mega menu -->

    <div class="menu">
        <!-- menu start -->
        <nav id="menu" class="mega-menu">
            <!-- menu list items container -->
            <section class="menu-list-items">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <!-- menu logo -->
                            <ul class="menu-logo">
                                <li>
                                    <a href="/"><img src="images/logo.png" alt="logo"> </a>
                                </li>
                            </ul>
                            <!-- menu links -->
                            <ul class="menu-links">
                                <!-- active class -->
                                <li class="active"><a href="/"> Home <i class=" fa-indicator"></i></a>

                                </li>

                                <li><a href="{{route('pricing')}}">Membership <i class=" fa-indicator"></i></a>

                                </li>
                                <li><a href="{{route('contact')}}">Contact <i class=" fa-indicator"></i></a>


                            </ul>

                        </div>
                    </div>
                </div>
            </section>
        </nav>
        <!-- menu end -->
    </div>
</header>