@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Queries</div>

                <div class="card-body">



                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr class="heading">
                                <th>No</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>City</th>
                                <th>Gender</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($customers as $customer)
                                <tr>
                                    <td>{{ $customer->id }}</td>
                                    <td>{!! $customer->name !!}</td>
                                    <td>{!! $customer->mobile !!}</td>
                                    <td>{!! $customer->city !!}</td>
                                    <td>{!! $customer->gender !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $customers->links() }}
                    </div>





                </div>
            </div>
        </div>
    </div>
</div>
@endsection
