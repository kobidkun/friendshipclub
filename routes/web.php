<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/pricing', 'PageManage@PricingPage')->name('pricing');
Route::get('/user-login', 'PageManage@LoginPage')->name('login');
Route::get('/contact', 'PageManage@ContactPage')->name('contact');
Route::get('/', 'PageManage@homePage')->name('home');
Route::get('/policy', 'PageManage@policy')->name('policy');
Route::post('/', 'PageManage@SaveForm')->name('form');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
