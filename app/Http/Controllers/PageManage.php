<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class PageManage extends Controller
{
    public function homePage(){
        return view('base.page.home');
    }

    public function ContactPage(){
        return view('base.page.contact');
    }

    public function LoginPage(){
        return view('base.page.login');
    }

    public function PricingPage(){
        return view('base.page.pricing');
    }

    public function policy(){
        return view('base.page.policy');
    }

    public function SaveForm(Request $request){
        $s = new Customer();
        $s->name = $request->name;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->city = $request->city;
        $s->gender = $request->gender;
       // dd($s);
        $s->save();

    }
}
